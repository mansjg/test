package pe.ferreyros.test;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by José Guadalupe Mandujano on 7/5/18.
 */
public class GithubFragment extends Fragment {


    private RecyclerView mView;
    private boolean isFirstLaunch = true;

    public static Fragment newInstance() {
        return new GithubFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (mView != null) {
            return mView;
        }
        mView = (RecyclerView) inflater.inflate(R.layout.github_fragment, container, false);
        return mView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isFirstLaunch) {
            isFirstLaunch = false;
            setupViews();
        }
    }

    private void setupViews() {

    }
}
