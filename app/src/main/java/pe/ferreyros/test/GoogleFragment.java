package pe.ferreyros.test;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by José Guadalupe Mandujano on 7/5/18.
 */
public class GoogleFragment extends Fragment {

    private View mView;
    private WebView webView;

    @NonNull
    public static Fragment newInstance() {
        return new GoogleFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (mView != null) {
            return mView;
        }
        mView = inflater.inflate(R.layout.google_fragment, container, false);
        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(webView == null) {
            webView = view.findViewById(R.id.webView);
            setupViews();
        }
    }

    private void setupViews() {
        WebViewClient client = new WebViewClient();
        webView.setWebViewClient(client);
//        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(Constants.GOOGLE_URL);
    }
}
