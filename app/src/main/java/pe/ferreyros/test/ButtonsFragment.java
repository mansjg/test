package pe.ferreyros.test;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by José Guadalupe Mandujano on 7/5/18.
 */
public class ButtonsFragment extends Fragment {


    private View mView;
    private Button button;
    private Button button2;

    public static Fragment newInstance() {
        return new ButtonsFragment();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (mView != null) {
            return mView;
        }
        mView = inflater.inflate(R.layout.buttons_fragment, container, false);
        return mView;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (button == null) {
            button = view.findViewById(R.id.button);
            button2 = view.findViewById(R.id.button2);
            setupViews();
        }
    }

    private void setupViews() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), R.string.toast, Toast.LENGTH_SHORT).show();
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(getContext())
                        .setTitle(android.R.string.dialog_alert_title)
                        .setMessage(R.string.alert)
                        .setPositiveButton(android.R.string.ok,null)
                        .show();
            }
        });
    }
}
